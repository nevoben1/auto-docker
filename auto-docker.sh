#!/usr/bin/env bash
##################################################################
# Created by : Nevo
# Purpose : creating several dockers and connect them to the same network
# Date : 13.3.22
# Version : 0.0.1
# set -x
####################################################################
function create_docker(){
echo "checking if the network exists"
docker network ls | grep $4
if [[ $? -eq 1 ]]; then	
	echo "no such network,creating..."
	docker network create $4
	echo "the network $4 has been crated"
fi
echo "creating containers please wait...."
sleep 3
i=0
while [[ $i -lt $3 ]]
do
	docker container run -d --name $1$i $2 
	let i++
done
echo "connecting created containers to the network $4 ..."
sleep 3
 
for i in `docker ps | grep $2 | awk '{print $1}'`; do
	`docker network connect $4 $i`
done
echo "the containers were created and connected to the requested net"
}


function help_func(){
echo "-d creates dockers , USAGE: -d [name] [image] [number of containers] [network name]"
echo "-n creates a network , USAGE: -n [name]"

}



####################################################################
while getopts ":dh" option;
do
	case $option in
	d) create_docker $2 $3 $4 $5
	;;
	h) help_func
	;;
	*) echo "invalid option - $OPTARG"
	;;
	esac
done
